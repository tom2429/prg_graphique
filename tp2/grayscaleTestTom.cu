#include <opencv2/opencv.hpp>

#include <vector>
#include <chrono>
// pour compiler nvcc -o grayscale grayscale.cu $(pkg-config --libs opencv)

__global__ void grayscale( unsigned char * rgb, unsigned char * g, std::size_t w, std::size_t h){
	auto tidx = blockIdx.x * blockDim.x + threadIdx.x;
	auto tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ( w < tidx && h < tidy){
		g[ tidy * w + tidx ] = 128;		
	}
}

int main()
{
  cv::Mat m_in = cv::imread("in.jpg", cv::IMREAD_UNCHANGED );
  auto rgb = m_in.data;
  
  std::vector< unsigned char > g( m_in.rows * m_in.cols );
  cv::Mat m_out( m_in.rows, m_in.cols, CV_8UC1, g.data() );

  auto w = m_in.cols;
  auto h = m_in.rows;

  unsigned char * rgb_d = nullptr;
  unsigned char * g_d = nullptr;

  cudaMalloc(&rgb_d, 3*w*h); //alloc image rgb
  cudaMalloc(&g_d,w*h); // alloc image g
  
  cudaMemcpy(rgb_d,rgb, 3*w*h, cudaMemcpyHostToDevice); // copie image rgb vers device

  dim3 block(32,32);
  dim3 grid( (w-1)/block.x+1, (h-1)/block.y+1 );
  grayscale<<<grid, block>>>( rgb_d, g_d, w,h);

  cudaMemcpy( g.data(), g_d, w*h, cudaMemcpyDeviceToHost ); // copie image gris vers host
/*
  auto start = std::chrono::system_clock::now();

  #pragma omp parallel for
  for( std::size_t j = 0 ; j < m_in.rows ; ++j )
    {
      for( std::size_t i = 0 ; i < m_in.cols ; ++i )
	{
	  g[ j * m_in.cols + i ] = (
			 307 * rgb[ 3 * ( j * m_in.cols + i ) ]
		       + 604 * rgb[ 3 * ( j * m_in.cols + i ) + 1 ]
		       + 113 * rgb[  3 * ( j * m_in.cols + i ) + 2 ]
		       ) / 1024;
	}
    }

  auto stop = std::chrono::system_clock::now();

  auto duration = stop - start;
  auto ms = std::chrono::duration_cast< std::chrono::milliseconds >( duration ).count();

  std::cout << ms << " ms" << std::endl;
 */
  cv::imwrite( "out.jpg", m_out );
  
  return 0;
}
