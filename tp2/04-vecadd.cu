#include <iostream>
#include <vector>


__global__ void vecadd( int * v0, int * v1, std::size_t size )
{
  // calcul de l'id global du thread
  auto tid = blockIdx.x*blockDim.x + threadIdx.x;

  // tid < 2000
  if (tid < size){
    v0[ tid ] += v1[ tid ];
  }

  // 16 threads qui ne font rien = 1/2 block de 32 threads
}


int main()
{
  std::vector< int > v0( 100 );
  std::vector< int > v1( 100 );
  
  int * v0_d = nullptr;
  int * v1_d = nullptr;

  for( std::size_t i = 0 ; i < v0.size() ; ++i )
  {
    v0[ i ] = v1[ i ] = i;
  }

  cudaMalloc( &v0_d, v0.size() * sizeof( int ) );
  cudaMalloc( &v1_d, v0.size() * sizeof( int ) );

  cudaMemcpy( v0_d, v0.data(), v0.size() * sizeof( int ), cudaMemcpyHostToDevice );
  cudaMemcpy( v1_d, v1.data(), v0.size() * sizeof( int ), cudaMemcpyHostToDevice );

// Taille du bloc : multiple de 32 jusqu'a 1024
  dim3 block(64);
  // size == 100 -> 1
  // size == 1024 -> 1023/1024+1 = 1
  // size == 2000 -> 1999/1024+1 = 2

  dim3 grid ((size-1)/block.x+1);
  vecadd<<< 1, 100 >>>( v0_d, v1_d, v0.size() );

  cudaMemcpy( v0.data(), v0_d, v0.size() * sizeof( int ), cudaMemcpyDeviceToHost );

  for( auto x: v0 )
  {
    std::cout << x << std::endl;
  }

  return 0;
}
