#include <iostream>
#include <vector>


__global__ void matadd( int * v0, int * v1, std::size_t w, std::size_t h)
{
  // calcul de l'id global du thread
  auto tidx = blockIdx.x*blockDim.x + threadIdx.x;
  auto tidy = blockIdx.y*blockDim.y + threadIdx.y;

  // tid < 2000
  if (tidx < w && tidy < h){
    v0[ w*tidy+tidx ] += v1[ w*tidy+tidx ];
  }
}


int main()
{
  std::size_t const w = 100;
  std::size_t const h = 100;
  std::size_t const size = w*h;
  std::vector< int > v0( size );
  std::vector< int > v1( size );
  
  int * v0_d = nullptr;
  int * v1_d = nullptr;

  for( std::size_t i = 0 ; i < v0.size() ; ++i )
  {
    v0[ i ] = v1[ i ] = i;
  }

  cudaMalloc( &v0_d, v0.size() * sizeof( int ) );
  cudaMalloc( &v1_d, v0.size() * sizeof( int ) );

  cudaMemcpy( v0_d, v0.data(), v0.size() * sizeof( int ), cudaMemcpyHostToDevice );
  cudaMemcpy( v1_d, v1.data(), v0.size() * sizeof( int ), cudaMemcpyHostToDevice );

// Taille du bloc : multiple de 32 jusqu'a 1024
  dim3 block(16,16); // blockDim 
  dim3 grid ((w-1)/block.x+1, (h-1)/block.y+1); // (4,4)
  matadd<<< 1, 100 >>>( v0_d, v1_d, w,h );

  cudaMemcpy( v0.data(), v0_d, v0.size() * sizeof( int ), cudaMemcpyDeviceToHost );

  for( auto x: v0 )
  {
    std::cout << x << std::endl;
  }

  return 0;
}
