#include <iostream>
#include <vector>


__global__ void fill( int * v, std::size_t size )
{
  auto tid = blockIdx.x*blockDim.x + threadIdx.x; // identifiant local du thread
  if (tid < size){
	  v[ tid ] = tid;
  }
}


int main()
{
  std::size_t const size = 2000;
  std::vector< int > v( size );

  int * v_d = nullptr;

  cudaMalloc( &v_d, v.size() * sizeof( int ) );
  
  // 1 block de size threads mais size <= 1024
  /**
    * <<< nblocs, taillebloc (blockDim) >>>
    */
  dim3 block(32); // 32 ou multiple de 32 <= 1024
  // si le nb d'elem n'est pas multiple du nb block alors 1 block en plus
  dim3 grid((size-1)/block.x+1);// (2000-1)/(32+1) = 63 mais 63*32 = 2016
  fill<<< grid, block >>>( v_d, v.size() );

  cudaMemcpy( v.data(), v_d, v.size() * sizeof( int ), cudaMemcpyDeviceToHost );

  for( auto x: v )
  {
    std::cout << x << std::endl;
  }

  return 0;
}
