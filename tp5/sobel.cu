#include <opencv2/opencv.hpp>
#include <vector>

__global__ void grayscale( unsigned char * rgb, unsigned char * g, std::size_t cols, std::size_t rows ) {
	
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;
  if( i < cols && j < rows ) {
    g[ j * cols + i ] = (
			 307 * rgb[ 3 * ( j * cols + i ) ]
			 + 604 * rgb[ 3 * ( j * cols + i ) + 1 ]
			 + 113 * rgb[  3 * ( j * cols + i ) + 2 ]
			 ) >> 10; // >>10 <=> division par 1024
  }
}

__global__ void sobel( unsigned char * g, unsigned char * s, std::size_t cols, std::size_t rows )
{
  auto i = blockIdx.x * blockDim.x + threadIdx.x;
  auto j = blockIdx.y * blockDim.y + threadIdx.y;

  if( i > 0 && i < cols-1 && j > 0 && j < rows-1 )
  {
    auto h = g[ (j-1)*cols + i - 1 ] - g[ (j-1)*cols + i + 1 ]
	    + 2*g[ j*cols + i - 1 ] - 2 * g[ j*cols + i + 1 ]
	    + g[ (j+1)*cols + i - 1 ] - g[ (j+1)*cols +i +1 ];
    auto v = g[ (j-1)*cols + i - 1 ] - g[ (j+1)*cols + i -1 ]
	    + 2*g[ (j-1)*cols+i  ] - 2 * g[ (j+1)*cols + i ]
	    + g[ (j-1)*cols + i +1 ] - g[ (j+1)*cols + i + 1 ];

    auto res = h*h+v*v;
    if ( res > 255*255 )
    {
      res = 255*255;
    }

    s[ j * cols + i ] = sqrtf( res );
  }
}


__global__ void grayscale_sobel_v0( unsigned char * rgb, unsigned char * g, unsigned char * s, std::size_t cols, std::size_t rows )
{
  auto i = blockIdx.x*blockDim.x + threadIdx.x;
  auto j = blockIdx.y*blockDim.y + threadIdx.y;

  if( i < cols && j < rows ) {
    g[ j * cols + i ] = (
			 307 * rgb[ 3 * ( j * cols + i ) ]
			 + 604 * rgb[ 3 * ( j * cols + i ) + 1 ]
			 + 113 * rgb[  3 * ( j * cols + i ) + 2 ]
			 ) >> 10; // >>10 <=> division par 1024
  }

  __syncthreads();

  if( i > 0 && i < cols-1 && j > 0 && j < rows-1 )
  {
    auto h = g[ (j-1)*cols + i - 1 ] - g[ (j-1)*cols + i + 1 ]
	    + 2*g[ j*cols + i - 1 ] - 2 * g[ j*cols + i + 1 ]
	    + g[ (j+1)*cols + i - 1 ] - g[ (j+1)*cols +i +1 ];
    auto v = g[ (j-1)*cols + i - 1 ] - g[ (j+1)*cols + i -1 ]
	    + 2*g[ (j-1)*cols+i  ] - 2 * g[ (j+1)*cols + i ]
	    + g[ (j-1)*cols + i +1 ] - g[ (j+1)*cols + i + 1 ];

    auto res = h*h+v*v;
    if ( res > 255*255 )
    {
      res = 255*255;
    }

    s[ j * cols + i ] = sqrtf( res );
  }
}


int main()
{
  cv::Mat m_in = cv::imread("in.jpg", cv::IMREAD_UNCHANGED );
  auto rgb = m_in.data;
  auto rows = m_in.rows;
  auto cols = m_in.cols;
  std::vector< unsigned char > g( rows * cols );
  cv::Mat m_out( rows, cols, CV_8UC1, g.data() );
  unsigned char * rgb_d;
  unsigned char * g_d;
  unsigned char * s_d;

  cudaEvent_t start, stop;

  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  cudaMalloc( &rgb_d, 3 * rows * cols );
  cudaMalloc( &g_d, rows * cols );
  cudaMalloc( &s_d, rows * cols );

  cudaMemcpy( rgb_d, rgb, 3 * rows * cols, cudaMemcpyHostToDevice );

  dim3 t( 32, 4 ); //128 threads
  dim3 b( ( cols - 1) / (t.x-2) + 1 , ( rows - 1 ) / (t.y-2) + 1 );

  cudaEventRecord(start);  

  /*  grayscale<<< b, t >>>( rgb_d, g_d, cols, rows );

  sobel<<< b, t >>>( g_d, s_d, cols, rows );
  */
  grayscale_sobel_v0<<< b, t, t.x*t.y >>>( rgb_d, g_d, s_d, cols, rows );

  //grayscale_sobel_v1<<< b, t, t.x*t.y >>>( rgb_d, s_d, cols, rows );

  
  cudaEventRecord(stop);
  
  cudaMemcpy( g.data(), s_d, rows * cols, cudaMemcpyDeviceToHost );

  cudaEventSynchronize( stop );

float duration;
cudaEventElapsedTime( &duration, start, stop );
std::cout << "time=" << duration << std::endl;

cudaEventDestroy(start);
cudaEventDestroy(stop);


  cv::imwrite( "out.jpg", m_out );
  cudaFree( rgb_d );
  cudaFree( g_d );
  cudaFree( s_d );
  return 0;
}
